tool
extends KinematicBody

export var speed = 14.0
var velocity = Vector3.ZERO

func _physics_process(delta):
	get_parent().get_node("MultiMeshInstance").material_override.set_shader_param("player_pos",global_transform.origin)
	
	var direction = Vector3.ZERO
	
	if Input.action_press("left"):
		direction.x -= 1	
	if Input.action_press("right"):
		direction.x += 1

	if direction != Vector3.ZERO :
		direction = direction.normalized()
		
	velocity.x = direction.x * speed
	velocity = move_and_slide(velocity, Vector3.UP)
