extends CanvasLayer

signal start_game


func _ready():
	$ScoreLabel.hide()

func update_score(score):
	$ScoreLabel.text = str(score)
	
func show_message(text):
	$MessageLabel.text = text 
	$MessageLabel.show()
	$MessageTimer.start()
	
func show_game_over():
	show_message("Game Over")
	yield($MessageTimer, "timeout")
	$ScoreLabel.hide()
	$MessageLabel.text = "Dodge the Creeps"
	$MessageLabel.show()
	
	yield(get_tree().create_timer(1.0),"timeout")
	$Button.show()
	$Button.set_toggle_mode(true)


func _on_Button_pressed():
	$Button.set_pressed_no_signal(true)
	$Button.set_toggle_mode(false)
	yield(get_tree().create_timer(0.25),"timeout")
	$Button.hide()
	emit_signal("start_game")


func _on_MessageTimer_timeout():
	$MessageLabel.hide()
