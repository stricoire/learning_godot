extends Area2D

signal hit

export var speed = 400

var screen_size = Vector2.ZERO
var player_size = Vector2.ZERO

func _ready():
	screen_size = get_viewport_rect().size
	player_size.x = get_node("CollisionShape2D").shape.get_radius()
	player_size.y = get_node("CollisionShape2D").shape.get_height()
	hide()

func _process(delta):
	var direction = Vector2.ZERO
	
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_up"):
		direction.y -= 1
	if Input.is_action_pressed("move_down"):
		direction.y += 1
		
	if direction.length() > 0:
		direction = direction.normalized()
		$AnimatedSprite.play()
	
	else:
		$AnimatedSprite.stop() 

	position += direction * speed * delta
	
	position.x = clamp(position.x, 0 + player_size.x, screen_size.x - player_size.x)
	position.y = clamp(position.y, 0 + player_size.y * 3, screen_size.y - player_size.y * 3)
	
	if direction.y != 0:
		$AnimatedSprite.animation = "up"
		$AnimatedSprite.flip_v = direction.y > 0
	
	elif direction.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_h = direction.x < 0
	
func start(new_position):
	position = new_position
	show()
	$CollisionShape2D.disabled = false


func _on_Player_body_entered(body):
	hide()
	$CollisionShape2D.set_deferred("disabled", true)
	emit_signal("hit")
	
