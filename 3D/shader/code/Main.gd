extends Node


onready var _viewport_container = get_node("ViewportContainer")
onready var _viewport = get_node("ViewportContainer/Viewport")
onready var _control_bitdepth = get_node("Control/TabContainer/ShaderModifier/BitDepth")
onready var _control_dithersize = get_node("Control/TabContainer/ShaderModifier/DitherSize")
onready var _control_contrast = get_node("Control/TabContainer/ShaderModifier/Contrast")
onready var _control_offset = get_node("Control/TabContainer/ShaderModifier/Offset")
onready var _palette_preview = get_node("Control/TabContainer/ShaderModifier/PalettePreview")
onready var _dither_preview = get_node("Control/TabContainer/ShaderModifier/DitherPreview")
onready var _dither_label = get_node("Control/TabContainer/ShaderModifier/DitherLabel")


onready var _dither_label_name = ["Bayer 16x16", "Bayer 8x8", "Bayer 4x4", "Bayer 2x2", "Blue Noise"]

export var palettes = []
export var dither_patterns = []

var idx_current_palette = null
var idx_current_dither_patterns = null

func _ready():
	_viewport.size = get_viewport().size
	$Control/TabContainer/ShaderModifier/Palette/HBoxContainer/Palette0.pressed = true
	_viewport_container.material = load("res://shader/ressources/ShaderMaterial_noEffect.tres")
	_control_bitdepth.setup("Bit Depth", 2, 64, 32, 1)
	_control_dithersize.setup("Dither Size", 1, 8, 2, 1)
	_control_contrast.setup("Contrast", 0.0, 5.0, 1.0, 0.01)
	_control_offset.setup("Offset", -1.0, 1.0, 0.0, 0.01)
	
	_palette_preview.texture = palettes[0]
	idx_current_palette = 0
	_dither_preview.texture = dither_patterns[0]
	idx_current_dither_patterns = 0
	_dither_label.text = "Bayer 16x16"

func _process(delta):
	_viewport_container.material.set_shader_param("u_bit_depth", int(_control_bitdepth.get_value()))
	_viewport_container.material.set_shader_param("u_dither_size", int(_control_dithersize.get_value()))
	_viewport_container.material.set_shader_param("u_contrast", _control_contrast.get_value())
	_viewport_container.material.set_shader_param("u_offset", _control_offset.get_value())


func _on_CheckButton_toggled(button_pressed):
	if button_pressed:
		_viewport_container.material = load("res://shader/ressources/ShaderMaterial_dither.tres")
	else:
		_viewport_container.material = load("res://shader/ressources/ShaderMaterial_noEffect.tres")
	$Control/TabContainer/ShaderModifier/ShaderActivation/CheckButton.release_focus()
	
	# Retrieve Palette
	_setup_palette(idx_current_palette)
	
	# Retrieve Dither
	_setup_dither(idx_current_dither_patterns)
	

func _on_Palette_pressed(index_palette):
	_setup_palette(index_palette)

func _on_Dither_pressed(index_dither_pattern):
	_setup_dither(index_dither_pattern)
	
func _setup_palette(index_palette):
	idx_current_palette = index_palette
	_viewport_container.material.set_shader_param("u_color_tex", palettes[index_palette])
	_palette_preview.texture = palettes[index_palette]
	get_node("Control/TabContainer/ShaderModifier/Palette/HBoxContainer/Palette"+str(index_palette)).release_focus()

	
func _setup_dither(index_dither_pattern):
	idx_current_dither_patterns = index_dither_pattern
	_viewport_container.material.set_shader_param("u_dither_tex", dither_patterns[index_dither_pattern])
	_dither_preview.texture = dither_patterns[index_dither_pattern]
	_dither_label.text = _dither_label_name[index_dither_pattern]
	get_node("Control/TabContainer/ShaderModifier/Dither/HBoxContainer/Dither"+str(index_dither_pattern)).release_focus()
	

func _on_Minimise_pressed():
	$Control/TabContainer.visible = not $Control/TabContainer.visible
	$Control/Minimise.release_focus()
