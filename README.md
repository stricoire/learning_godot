# Lerning Godot

This a compilation of projects I used to begin to learn Godot Engine. <br>
In this repository you'll find complete access to my source code.


<b>Learning 2D <br> </b>
I completed the ["First 2D game" tutorial](https://gdquest.com/tutorial/godot/) from [@NathanGDQuest](https://twitter.com/NathanGDQuest).

It already covers a wide range of subject 📚 <br>
Can't resist to tweek a bit the tuto using my personal touch 🎨

Basic of Godot 2D ✅

<figure>
  <img src="Pictures/2D.png"
       width="250">
</figure>

---

<b>Learning 3D <br> </b>

I completed the ["First 3D game" tutorial](https://gdquest.com/tutorial/godot/) from [@NathanGDQuest](https://twitter.com/NathanGDQuest). <br>
I also experimented with shader and UI, using the [dithering shader](https://github.com/samuelbigos/godot_dither_shader#how-to-use) from [@Calneon](https://twitter.com/Calneon)
 and combined the two.

Basic of Godot 3D ✅ <br>
Basic of shaders ✅ <br>
Basic of UI ✅

<figure>
  <img src="Pictures/3D.png"
       width="500">
</figure>

---

<b>Experimenting a bit more with shaders <br> </b>

This time I tried to implement a small ray-tracing algorithm into frag-shader, based on what I remembered from CG courses (and Internet stuff).

<figure>
  <img src="Pictures/Shader_raytracer.png"
       width="500">
</figure>
